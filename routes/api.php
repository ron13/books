<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')->group(function () {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::get('me', 'AuthController@me');
});

Route::apiResource('books', 'BookController');
Route::prefix('books')->group(function () {
    Route::post('/import', 'BookController@import');
});

Route::apiResource('authors', 'AuthorController')->only([
    'index',
    'show',
]);

Route::apiResource('publishers', 'PublisherController')->only([
    'index',
    'show',
]);
