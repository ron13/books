<?php

namespace Tests\Unit;

use App\Author;
use App\Book;
use App\Exceptions\UnknownImportFileTypeException;
use App\Log;
use App\Publisher;
use App\Services\Books\BookService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

/**
 * Class BookServiceTest
 * @package Tests\Unit
 */
class BookServiceTest extends TestCase
{

    /**
     * @var BookService
     */
    private $service;

    /**
     * BookServiceTest constructor.
     * @param null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->service = new BookService();
    }

    public function testValidationValid()
    {
        $attributes = [
            'name'       => 'Hello world',
            'isbn'       => '999-9-99-999999-9',
            'page_count' => 100,
            'authors'    => Author::limit(2)->pluck('id')->all(),
            'publisher'  => Publisher::first()->id,
        ];
        $validator = BookService::getValidator($attributes);

        $this->assertFalse($validator->fails());
    }

    public function testValidationFail()
    {
        $attributes = [
            'name'       => '',
            'isbn'       => '999-9-99-999999-97',
            'page_count' => 0,
            'authors'    => [99999999],
            'publisher'  => 9999999,
        ];
        $validator = BookService::getValidator($attributes);
        $result = $validator->errors();

        $this->assertEquals([
            "name" => [
                "The name field is required."
            ],
            "isbn" => [
                "The isbn must be 17 characters.",
                "The isbn format is invalid.",
            ],
            "page_count" => [
                "The page count must be at least 3.",
            ],
            "publisher" => [
                "The selected publisher is invalid.",
            ],
            "authors.0" => [
                "The selected authors.0 is invalid.",
            ],
        ], $result->toArray());
    }

    /**
     * Test create book
     */
    public function testCreateBook()
    {
        $logCount = Log::count();
        $attributes = [
            'name'       => 'Hello world',
            'isbn'       => '999-9-99-999999-9',
            'page_count' => 100,
            'authors'    => Author::limit(2)->pluck('id')->all(),
            'publisher'  => Publisher::first()->id,
        ];
        $this->service->createBook($attributes);

        $this->assertDatabaseHas('books', [
            'name'       => 'Hello world',
            'isbn'       => '999-9-99-999999-9',
            'page_count' => 100,
        ]);
        $this->assertEquals($logCount + 1, Log::count());
    }

    /**
     * Test update the book
     */
    public function testUpdateBook()
    {
        $logCount = Log::count();
        $book     = Book::find(1);

        $attributes = [
            'name'       => 'Hello world',
            'isbn'       => '999-9-99-999999-8',
            'page_count' => 99,
            'authors'    => Author::limit(2)->pluck('id')->all(),
            'publisher'  => Publisher::first()->id,
        ];
        $this->service->updateBook($book, $attributes);

        $this->assertDatabaseHas('books', [
            'name'       => 'Hello world',
            'isbn'       => '999-9-99-999999-8',
            'page_count' => 99,
        ]);
        $this->assertEquals($logCount + 1, Log::count());
    }

    /**
     * Test delete the book
     */
    public function testDeleteBook()
    {
        $logCount = Log::count();
        $book     = Book::find(1);

        $this->service->deleteBook($book);

        $this->assertDatabaseMissing('books', [
            'id' => 1,
        ]);
        $this->assertEquals($logCount + 1, Log::count());
    }

    /**
     * Test import from csv
     *
     * @return void
     * @throws UnknownImportFileTypeException
     */
    public function testImportCsv()
    {
        $logCount = Log::count();
        $filePath = storage_path('app/import/books.csv');
        $file     = new File($filePath);
        $count    = $this->service->import($file, $file->getExtension())->count();

        $this->assertEquals($count, 3);
        $this->assertEquals($logCount + 3, Log::count());
    }

    /**
     * Test import from csv
     *
     * @return void
     * @throws UnknownImportFileTypeException
     */
    public function testImportJson()
    {
        $logCount = Log::count();
        $filePath = storage_path('app/import/books.json');
        $file     = new File($filePath);
        $count    = $this->service->import($file, $file->getExtension())->count();

        $this->assertEquals($count, 3);
        $this->assertEquals($logCount + 3, Log::count());
    }
}
