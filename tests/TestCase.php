<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Artisan;

/**
 * Class TestCase
 * @package Tests
 */
abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public const AUTH_USERNAME = 'john.doe@mail.com';
    public const AUTH_PASSWORD = 'password';

    public static $token;

    protected function setUp(): void
    {
        parent::setUp();
        Artisan::call('migrate');
        $this->seed();
        if (is_null(static::$token)) {
            static::$token = auth()->attempt([
                'email'    => self::AUTH_USERNAME,
                'password' => self::AUTH_PASSWORD,
            ]);
        }
    }
}
