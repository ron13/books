<?php

namespace Tests\Feature;

use App\Author;
use App\Publisher;
use Illuminate\Http\Testing\File;
use Tests\TestCase;

/**
 * Class ExampleTest
 * @package Tests\Feature
 */
class BookTest extends TestCase
{

    public function testIndex()
    {
        $response = $this->json('get', '/api/books', [], [
            'Authorization' => "bearer " . self::$token,
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data',
            'meta' => [
                'current_page',
                'from',
                'last_page',
                'path',
                'per_page',
                'to',
                'total',
            ],
        ]);
    }

    public function testShow()
    {
        $response = $this->json('get', '/api/books/1', [], [
            'Authorization' => "bearer " . self::$token,
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'isbn',
                'page_count',
                'authors' => [
                    [
                        'id',
                        'name',
                    ],
                ],
                'publisher' => [
                    'id',
                    'name',
                    'url',
                ],
            ],
        ]);
    }

    public function testCreate()
    {
        $response = $this->json('post', '/api/books', [
            'name'       => 'Hello world',
            'isbn'       => '999-9-99-999999-9',
            'page_count' => 100,
            'authors'    => Author::limit(2)->pluck('id')->all(),
            'publisher'  => Publisher::first()->id,
        ], [
            'Authorization' => "bearer " . self::$token,
        ]);

        $response->assertStatus(201);
        $response->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'isbn',
                'page_count',
                'authors' => [
                    [
                        'id',
                        'name',
                    ],
                ],
                'publisher' => [
                    'id',
                    'name',
                    'url',
                ],
            ],
        ]);
        $this->assertDatabaseHas('books', [
            'name'       => 'Hello world',
            'isbn'       => '999-9-99-999999-9',
            'page_count' => 100,
        ]);
    }

    public function testUpdate()
    {
        $response = $this->json('put', '/api/books/1', [
            'name'       => 'Hello world',
            'isbn'       => '999-9-99-999999-8',
            'page_count' => 100,
            'authors'    => Author::limit(2)->pluck('id')->all(),
            'publisher'  => Publisher::first()->id,
        ], [
            'Authorization' => "bearer " . self::$token,
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'isbn',
                'page_count',
                'authors' => [
                    [
                        'id',
                        'name',
                    ],
                ],
                'publisher' => [
                    'id',
                    'name',
                    'url',
                ],
            ],
        ]);
        $this->assertDatabaseHas('books', [
            'name'       => 'Hello world',
            'isbn'       => '999-9-99-999999-8',
            'page_count' => 100,
        ]);
    }

    public function testDelete()
    {
        $response = $this->json('delete', '/api/books/1', [], [
            'Authorization' => "bearer " . self::$token,
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'isbn',
                'page_count',
                'authors' => [
                    [
                        'id',
                        'name',
                    ],
                ],
                'publisher' => [
                    'id',
                    'name',
                    'url',
                ],
            ],
        ]);
        $this->assertDatabaseMissing('books', [
            'id' => 1,
        ]);
    }

    public function testImportJson()
    {
        $file = fopen(storage_path('app/import/books.json'), 'r');
        $response = $this->json('post', '/api/books/import', [
            'source' => new File('books.json', $file),
        ], [
            'Authorization' => "bearer " . self::$token,
        ]);

        $response->assertStatus(200);
        $response->assertExactJson([
            'import_count' => 3,
        ]);
    }

    public function testImportCsv()
    {
        $file = fopen(storage_path('app/import/books.csv'), 'r');
        $response = $this->json('post', '/api/books/import', [
            'source' => new File('books.csv', $file),
        ], [
            'Authorization' => "bearer " . self::$token,
        ]);

        $response->assertStatus(200);
        $response->assertExactJson([
            'import_count' => 3,
        ]);
    }
}
