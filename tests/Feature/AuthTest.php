<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

/**
 * Class ExampleTest
 * @package Tests\Feature
 */
class AuthTest extends TestCase
{

    public function testLoginFail()
    {
        $response = $this->json('post', '/api/auth/login', [
            'email'    => 'admin@admin.com',
            'password' => self::AUTH_PASSWORD,
        ]);

        $response->assertStatus(401);
        $response->assertJsonStructure([
            'error',
        ]);
    }

    public function testLogin()
    {
        $response = $this->json('post', '/api/auth/login', [
            'email'    => self::AUTH_USERNAME,
            'password' => self::AUTH_PASSWORD,
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'access_token',
            'token_type',
            'expires_in',
        ]);

        static::$token = $response->decodeResponseJson('access_token');
    }

    public function testMe()
    {
        $response = $this->json('get', '/api/auth/me', [], [
            'Authorization' => "bearer " . static::$token,
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                'id', 'name', 'email',
            ]
        ]);
    }

    public function testRefresh()
    {
        $response = $this->json('post', '/api/auth/refresh', [], [
            'Authorization' => "bearer " . static::$token,
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'access_token',
            'token_type',
            'expires_in',
        ]);
    }

    public function testLogout()
    {
        $response = $this->json('post', '/api/auth/logout', [], [
            'Authorization' => "bearer " . static::$token,
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'message',
        ]);
    }
}
