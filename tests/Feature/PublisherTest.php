<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

/**
 * Class ExampleTest
 * @package Tests\Feature
 */
class PublisherTest extends TestCase
{

    public function testIndex()
    {
        $response = $this->json('get', '/api/publishers', [], [
            'Authorization' => "bearer " . self::$token,
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data',
            'meta' => [
                'current_page',
                'from',
                'last_page',
                'path',
                'per_page',
                'to',
                'total',
            ],
        ]);
    }

    public function testShow()
    {
        $response = $this->json('get', '/api/publishers/1', [], [
            'Authorization' => "bearer " . self::$token,
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'url',
            ],
        ]);
    }
}
