#Books
Books organizer

##Requirements
###For quick start
* Docker >= 18
* Docker-compose >= 1.21

###For manual setup
* MySQL >= 8
* composer >= 1.9
* nginx or apache server
* SQLite (for tests)
* PHP >= 7.1.3
* BCMath PHP Extension
* Ctype PHP Extension
* JSON PHP Extension
* Mbstring PHP Extension
* OpenSSL PHP Extension
* PDO PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension
* MySQL PHP Extension
* SQLite PHP Extension (for tests)


##Installation and run
###Quick start
Run `docker-compose up` in your command line. It will configure and run all required docker containers and if .env file does not exists it also will:
* create .env file
* migrate database structure
* seed dummy data to the database

###Manual setup
* `composer install`
* `cp .env.example .env`
* update database connection credentials in the .env file
* `php artisan key:generate --ansi`
* `php artisan jwt:secret`
* `php artisan migrate:fresh` (use `--seed` parameter for dummy data setup) 

##API Documentation
API documentation will be available under http://localhost/api/documentation

##GraphQL
GraphQL requests will be available under http://localhost/graphql

##Authentication/Authorization
You can use next auth credentials if dummy data installed:
```
{
  "email": "john.doe@mail.com",
  "password": "password"
}
```
To authorize your requests use HTTP header `Authorization: bearer {access_token}`

##Import books
`storage/app/import` directory contains the import file examples

For import via command line interface:
```
php artisan import {path/to/file}
```
