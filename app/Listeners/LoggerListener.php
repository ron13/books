<?php

namespace App\Listeners;

use App\Events\BookEvent;
use App\Exceptions\UnsupportedLogEventException;
use App\Services\Logs\LogService;

/**
 * Class LoggerListener
 * @package App\Listeners
 */
class LoggerListener
{
    /**
     * @var LogService
     */
    private $service;

    /**
     * Create the event listener.
     *
     * @param LogService $service
     */
    public function __construct(LogService $service)
    {
        $this->service = $service;
    }

    /**
     * Handle the event.
     *
     * @param BookEvent $event
     * @return void
     * @throws UnsupportedLogEventException
     */
    public function handle(BookEvent $event)
    {
        $this->service->createLog($event->getType(), $event->getModel());
    }
}
