<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Log
 * @package App
 */
class Log extends Model
{
    private const CAST_OBJECT = 'object';

    protected $fillable = [
        'user',
        'data',
        'event',
    ];

    protected $casts = [
        'user'    => self::CAST_OBJECT,
        'data'    => self::CAST_OBJECT,
    ];

    /**
     * Get the owning loggable model.
     */
    public function loggable()
    {
        return $this->morphTo('loggable');
    }
}
