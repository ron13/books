<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Author
 * @package App
 */
class Author extends Model
{
    /**
     * Books of the author
     *
     * @return BelongsToMany
     */
    public function books()
    {
        return $this->belongsToMany(Book::class, 'author_books');
    }
}
