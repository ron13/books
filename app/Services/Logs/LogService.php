<?php

namespace App\Services\Logs;

use App\Book;
use App\Events\BookEvent;
use App\Exceptions\UnsupportedLogEventException;
use App\Http\Resources\BookResource;
use App\Http\Resources\UserResource;
use App\Log;
use Illuminate\Database\Eloquent\Model;

/**
 * Class LogService
 * @package App\Services\Logs
 */
class LogService
{
    public const AVAILABLE_EVENTS = [
        BookEvent::TYPE_DELETED,
        BookEvent::TYPE_UPDATED,
        BookEvent::TYPE_CREATED,
    ];

    private $resourceMapper = [
        Book::class => BookResource::class,
    ];

    /**
     * @param string $event
     * @param $data
     * @throws UnsupportedLogEventException
     */
    public function createLog(string $event, Model $data)
    {
        $this->validateEventType($event);
        /** @var Log $log */
        $log = Log::make([
            'user'    => auth()->check() ? new UserResource(auth()->user()) : null,
            'data'    => $this->getResource($data),
            'event'   => $event,
        ]);
        $log->loggable()->associate($data);
        $log->save();
    }

    /**
     * @param string $event
     * @throws UnsupportedLogEventException
     */
    private function validateEventType(string $event)
    {
        if (!in_array($event, self::AVAILABLE_EVENTS)) {
            throw new UnsupportedLogEventException();
        }
    }

    /**
     * Get resource for log data
     *
     * @param $data
     * @return array
     */
    private function getResource(Model $data)
    {
        if (isset($this->resourceMapper[get_class($data)])) {
            return new $this->resourceMapper[get_class($data)]($data);
        } else {
            return $data->toArray();
        }
    }

}
