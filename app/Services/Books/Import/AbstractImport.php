<?php

namespace App\Services\Books\Import;

use App\Author;
use App\Book;
use App\Publisher;
use App\Services\Books\BookService;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Class AbstractImport
 * @package App\Services\Books\Import
 */
abstract class AbstractImport
{
    /**
     * @var File
     */
    protected $source;
    /**
     * @var BookService
     */
    private $service;

    /**
     * AbstractImport constructor.
     * @param File $source
     */
    public function __construct(File $source)
    {
        $this->source = $source;
        $this->service = new BookService();
    }

    /**
     * Read items from source
     *
     * @return Collection
     */
    abstract public function readSource(): Collection;

    /**
     * Create book instance
     *
     * @param string $name
     * @param string $isbn
     * @param int $pageCount
     * @param array $authors
     * @param string $publisher
     * @return array|bool
     */
    protected function makeBook(string $name, string $isbn, int $pageCount, array $authors, string $publisher)
    {
        /** @var \Illuminate\Database\Eloquent\Collection $bookAuthors */
        $bookAuthors = Author::whereIn('name', $authors)->get();
        /** @var Publisher $bookPublisher */
        $bookPublisher = Publisher::where('name', $publisher)->first();

        try {
            BookService::getValidator([
                'name'        => $name,
                'isbn'        => $isbn,
                'page_count' => $pageCount,
                'authors'    => $bookAuthors->pluck('id')->all(),
                'publisher'  => $bookPublisher ? $bookPublisher->id : 0,
            ])->validate();

            return [
                'name'        => $name,
                'isbn'        => $isbn,
                'page_count' => $pageCount,
                'authors'    => $bookAuthors->pluck('id')->all(),
                'publisher'  => $bookPublisher ? $bookPublisher->id : 0,
            ];
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Run import process
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function run()
    {
        $books = new \Illuminate\Database\Eloquent\Collection();
        if ($items = $this->readSource()) {
            DB::beginTransaction();
            /** @var Book $item */
            foreach ($items as $item) {
                $book = $this->service->createBook($item);
                $books->add($book);
            }
            DB::commit();

            return $books;
        }

        return $books;
    }
}
