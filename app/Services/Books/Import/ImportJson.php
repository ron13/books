<?php

namespace App\Services\Books\Import;

use Illuminate\Support\Collection;

/**
 * Class ImportJson
 * @package App\Services\Books\Import
 */
class ImportJson extends AbstractImport
{

    /**
     * Read items from source
     *
     * @return Collection
     */
    public function readSource(): Collection
    {
        $data = json_decode(file_get_contents($this->source->getRealPath()), true);

        $books = new Collection();
        foreach ($data as $datum) {
            if (!isset($datum['name'], $datum['isbn'], $datum['page_count'], $datum['author'], $datum['publisher'])) {
                continue;
            }

            $book = $this->makeBook(
                $datum['name'],
                $datum['isbn'],
                $datum['page_count'],
                $datum['author'],
                $datum['publisher']
            );

            if ($book) {
                $books->add($book);
            }
        }

        return $books;
    }
}
