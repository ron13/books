<?php

namespace App\Services\Books\Import;

use Illuminate\Support\Collection;

/**
 * Class ImportCsv
 * @package App\Services\Books\Import
 */
class ImportCsv extends AbstractImport
{

    /**
     * Read items from source
     *
     * @return Collection
     */
    public function readSource(): Collection
    {
        $file = fopen($this->source->getRealPath(), 'r');

        $head = fgetcsv($file);
        $columns = [];
        foreach ($head as $index => $column) {
            $columns[$column] = $index;
        }
        extract($columns);

        $books = new Collection();
        while ($data = fgetcsv($file)) {
            if (!isset($name, $isbn, $page_count, $author, $publisher)) {
                continue;
            }

            $book = $this->makeBook(
                $data[$name],
                $data[$isbn],
                $data[$page_count],
                explode(',', $data[$author]),
                $data[$publisher]
            );

            if ($book) {
                $books->add($book);
            }
        }
        fclose($file);

        return $books;
    }
}
