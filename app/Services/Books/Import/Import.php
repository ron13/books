<?php

namespace App\Services\Books\Import;

use App\Exceptions\UnknownImportFileTypeException;
use Illuminate\Support\Collection;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Class Import
 * @package App\Services\Books\Import
 */
class Import extends AbstractImport
{
    /**
     * @var AbstractImport
     */
    private $processor;

    /**
     * Import constructor.
     * @param File $source
     * @param $type
     * @throws UnknownImportFileTypeException
     */
    public function __construct(File $source, $type)
    {
        parent::__construct($source);

        switch ($type) {
            case 'csv':
                $this->processor = new ImportCsv($source);
                break;
            case 'json':
                $this->processor = new ImportJson($source);
                break;
            default:
                throw new UnknownImportFileTypeException();
                break;
        }
    }

    /**
     * Read items from source
     *
     * @return Collection
     */
    public function readSource(): Collection
    {
        return $this->processor->readSource();
    }
}
