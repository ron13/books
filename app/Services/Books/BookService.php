<?php

namespace App\Services\Books;

use App\Author;
use App\Book;
use App\Events\BookEvent;
use App\Exceptions\UnknownImportFileTypeException;
use App\Publisher;
use App\Services\Books\Import\Import;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Class BookService
 * @package App\Services\Books
 */
class BookService
{
    public const FIELD_PAGE_COUNT = 'page_count';
    public const FIELD_PUBLISHER  = 'publisher';
    public const FIELD_ISBN       = 'isbn';
    public const FIELD_NAME       = 'name';
    public const FIELD_AUTHORS    = 'authors';

    /**
     * Make new Book instance
     *
     * @param array $attributes
     * @return Book
     */
    public function makeBook(array $attributes): Book
    {
        /** @var Book $book */
        $book = Book::make([
            self::FIELD_NAME       => $attributes[self::FIELD_NAME],
            self::FIELD_ISBN       => $attributes[self::FIELD_ISBN],
            self::FIELD_PAGE_COUNT => $attributes[self::FIELD_PAGE_COUNT],
        ]);

        if (
            isset($attributes[self::FIELD_PUBLISHER])
            && $publisher = Publisher::find($attributes[self::FIELD_PUBLISHER])
        ) {
            $book->publisher()->associate($publisher);
        }

        return $book;
    }

    /**
     * Make and store new Book instance
     *
     * @param array $attributes
     * @return Book
     */
    public function createBook(array $attributes)
    {
        $book = $this->makeBook($attributes);

        $result = $book->save();

        if (
            isset($attributes[self::FIELD_AUTHORS])
            && ($authors = Author::whereIn('id', $attributes[self::FIELD_AUTHORS])->get())->isNotEmpty()
        ) {
            $book->authors()->sync($authors);
        }

        if ($result) {
            event(new BookEvent(BookEvent::TYPE_CREATED, $book));
        }

        return $book;
    }

    /**
     * Update existent Book instance
     *
     * @param Book $book
     * @param array $attributes
     * @return Book
     */
    public function updateBook(Book $book, array $attributes)
    {
        $book->fill([
            self::FIELD_NAME       => $attributes[self::FIELD_NAME],
            self::FIELD_ISBN       => $attributes[self::FIELD_ISBN],
            self::FIELD_PAGE_COUNT => $attributes[self::FIELD_PAGE_COUNT],
        ]);
        $book->publisher()->associate($attributes[self::FIELD_PUBLISHER]);
        $result = $book->save();

        if (
            isset($attributes[self::FIELD_AUTHORS])
            && ($authors = Author::whereIn('id', $attributes[self::FIELD_AUTHORS])->get())->isNotEmpty()
        ) {
            $book->authors()->sync($authors);
        }

        if ($result) {
            event(new BookEvent(BookEvent::TYPE_UPDATED, $book));
        }

        return $book;
    }

    /**
     * @param Book $book
     * @return bool
     * @throws \Exception
     */
    public function deleteBook(Book $book): bool
    {
        $result = $book->delete();

        if ($result) {
            event(new BookEvent(BookEvent::TYPE_DELETED, $book));
        }

        return $result;
    }

    /**
     * Import Books from file
     *
     * @param File $file
     * @param string $type
     * @return Collection
     * @throws UnknownImportFileTypeException
     */
    public function import(File $file, string $type)
    {
        $import = new Import($file, $type);

        return $import->run();
    }

    /**
     * Get Book validator
     *
     * @param array $attributes
     * @param bool $update
     * @return \Illuminate\Validation\Validator
     */
    public static function getValidator(array $attributes, $update = false): \Illuminate\Contracts\Validation\Validator
    {
        $uniqueIsbn = Rule::unique('books', self::FIELD_ISBN);
        if ($update) {
            $uniqueIsbn->ignore($attributes[self::FIELD_ISBN], self::FIELD_ISBN);
        }

        return Validator::make($attributes, [
            self::FIELD_NAME         => 'required|string',
            self::FIELD_ISBN         => [
                'required',
                'string',
                'size:17',
                'regex:/^[\d]{3}-[\d]-[\d]{2}-[\d]{6}-[\d]$/',
                $uniqueIsbn,
            ],
            self::FIELD_PAGE_COUNT     => 'required|integer|min:3',
            self::FIELD_AUTHORS        => 'required|array',
            self::FIELD_AUTHORS . '.*' => 'required|int|exists:authors,id',
            self::FIELD_PUBLISHER      => 'required|int|exists:publishers,id',
        ]);
    }
}
