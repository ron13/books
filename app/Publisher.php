<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property integer id
 */
class Publisher extends Model
{

    /**
     * Books of the publisher
     *
     * @return HasMany
     */
    public function books()
    {
        return $this->hasMany(Book::class);
    }
}
