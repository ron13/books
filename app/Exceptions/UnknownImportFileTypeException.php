<?php

namespace App\Exceptions;

use Exception;
use Throwable;

/**
 * Class UnknownImportFileTypeException
 * @package App\Exceptions
 */
class UnknownImportFileTypeException extends Exception
{
    /**
     * UnknownImportFileTypeException constructor.
     * @param null $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = null, $code = 422, Throwable $previous = null)
    {
        $message = $message ?? __('Unknown import file format.');
        parent::__construct($message, $code, $previous);
    }
}
