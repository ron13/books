<?php

namespace App\Exceptions;

use Exception;
use Throwable;

/**
 * Class UnsupportedLogEventException
 * @package App\Exceptions
 */
class UnsupportedLogEventException extends Exception
{
    /**
     * UnsupportedLogEventException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        $message = $message ?? __('Unsupported log event type.');
        parent::__construct($message, $code, $previous);
    }
}
