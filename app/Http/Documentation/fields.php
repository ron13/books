<?php

/**
 * @OA\Schema(
 *     schema="ListMeta",
 *     description="Meta information about request",
 *     @OA\Property(
 *          property="current_page",
 *          type="integer",
 *     ),
 *     @OA\Property(
 *          property="from",
 *          type="integer",
 *     ),
 *     @OA\Property(
 *          property="last_page",
 *          type="integer",
 *     ),
 *     @OA\Property(
 *          property="path",
 *          type="string",
 *     ),
 *     @OA\Property(
 *          property="per_page",
 *          type="integer",
 *     ),
 *     @OA\Property(
 *          property="to",
 *          type="integer",
 *     ),
 *     @OA\Property(
 *          property="total",
 *          type="integer",
 *     )
 * )
 *
 * @OA\Schema(
 *     schema="NotNound",
 *     description="Resource not found",
 *     @OA\Property(
 *          property="message",
 *          type="string",
 *          example="No query results for model",
 *     ),
 * )
 *
 * @OA\Schema(
 *     schema="UnprocessableEntity",
 *     description="Invalid request parameters",
 *     @OA\Property(
 *          property="message",
 *          type="string",
 *          example="The given data was invalid.",
 *     ),
 *     @OA\Property(
 *          property="errors",
 *          type="object",
 *          @OA\Property(
 *               property="{field_name}",
 *               type="array",
 *               @OA\Items(type="string", example="{Field validation error}"),
 *          )
 *     )
 * )
 *
 * @OA\Schema(
 *     schema="Import",
 *     description="Import request parameters",
 *     required={"source"},
 *     @OA\Property(
 *         description="Import data file. One of: csv, json",
 *         property="source",
 *         type="file",
 *     ),
 * )
 *
 * @OA\Schema(
 *     schema="ImportResponse",
 *     description="Information about imported data",
 *     @OA\Property(
 *          property="import_count",
 *          description="Number of imported items",
 *          type="integer",
 *     ),
 * )
 *
 * @OA\Schema(
 *     schema="Autenticated",
 *     description="User is successfuly authenticated",
 *     @OA\Property(
 *          property="access_token",
 *          type="string",
 *     ),
 *     @OA\Property(
 *          property="token_type",
 *          description="Token",
 *          type="string",
 *          example="bearer",
 *     ),
 *     @OA\Property(
 *          property="expires_in",
 *          description="Expiration time in seconds",
 *          type="integer",
 *          example="3600",
 *     ),
 * )
 *
 * @OA\Schema(
 *     schema="Unauthenticated",
 *     description="User is unauthenticated",
 *     @OA\Property(
 *          property="error",
 *          type="string",
 *          example="Unauthenticated",
 *     ),
 * )
 *
 * @OA\Schema(
 *     schema="Unauthorized",
 *     description="User is unauthorized",
 *     @OA\Property(
 *          property="error",
 *          type="string",
 *          example="Unauthorized",
 *     ),
 * )
 *
 * @OA\Schema(
 *     schema="LoggedOut",
 *     description="User is logged out",
 *     @OA\Property(
 *          property="message",
 *          type="string",
 *          example="Successfully logged out",
 *     ),
 * )
 */
