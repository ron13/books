<?php

/**
 * @OA\SecurityScheme(
 *   securityScheme="bearerAuth",
 *   type="http",
 *   in="header",
 *   scheme="bearer"
 * )
 */
