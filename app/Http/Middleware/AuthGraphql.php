<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Nuwave\Lighthouse\Exceptions\AuthenticationException;
use Nuwave\Lighthouse\Exceptions\AuthorizationException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

/**
 * Class AuthGraphql
 * @package App\Http\Middleware
 */
class AuthGraphql extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param \Closure $next
     * @return mixed
     * @throws AuthenticationException
     * @throws AuthorizationException
     */
    public function handle($request, Closure $next)
    {
        $this->authenticate($request);

        if (!auth()->check()) {
            throw new AuthenticationException();
        }
        return $next($request);
    }

    /**
     * @param Request $request
     * @throws AuthorizationException
     */
    public function authenticate(Request $request)
    {
        try {
            parent::authenticate($request);
        } catch (UnauthorizedHttpException $e) {
            throw new AuthorizationException();
        }
    }
}
