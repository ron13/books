<?php

namespace App\Http\Controllers;

use App\Author;
use App\Http\Resources\AuthorCollectionResource;
use App\Http\Resources\AuthorResource;

/**
 * Class AuthorController
 * @package App\Http\Controllers
 */
class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return AuthorCollectionResource
     *
     * @OA\Get(
     *     path="/api/authors",
     *     tags={"Authors"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(name="page", in="query", @OA\Schema(type="integer"), description="Number of required page"),
     *     @OA\Response(
     *         response="200",
     *         description="List of authors",
     *         @OA\JsonContent(ref="#/components/schemas/Authors")
     *     ),
     *     @OA\Response(
     *         response="403",
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/Unauthorized")
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthenticated",
     *         @OA\JsonContent(ref="#/components/schemas/Unauthenticated")
     *     ),
     * )
     */
    public function index()
    {
        return new AuthorCollectionResource(Author::paginate());
    }

    /**
     * Display the specified resource.
     *
     * @param Author $author
     * @return AuthorResource
     *
     * @OA\Get(
     *     path="/api/authors/{author_id}",
     *     tags={"Authors"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="author_id",
     *         required=true,
     *         in="path",
     *         description="ID of required author",
     *         @OA\Schema(type="integer"),
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Author",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                 property="data",
     *                 ref="#/components/schemas/Author"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Publisher not found",
     *         @OA\JsonContent(ref="#/components/schemas/NotNound")
     *     ),
     *     @OA\Response(
     *         response="403",
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/Unauthorized")
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthenticated",
     *         @OA\JsonContent(ref="#/components/schemas/Unauthenticated")
     *     ),
     * )
     */
    public function show(Author $author)
    {
        return new AuthorResource($author);
    }
}
