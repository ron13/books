<?php

namespace App\Http\Controllers;

use App\Book;
use App\Exceptions\UnknownImportFileTypeException;
use App\Http\Requests\ImportBooksRequest;
use App\Http\Requests\StoreBookRequest;
use App\Http\Resources\BookCollectionResource;
use App\Http\Resources\BookResource;
use App\Services\Books\BookService;
use Exception;

/**
 * Class BookController
 * @package App\Http\Controllers
 */
class BookController extends Controller
{
    /**
     * @var BookService
     */
    private $service;

    /**
     * BookController constructor.
     * @param BookService $service
     */
    public function __construct(BookService $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return BookCollectionResource
     *
     * @OA\Get(
     *     path="/api/books",
     *     tags={"Books"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(name="page", in="query", @OA\Schema(type="integer"), description="Number of required page"),
     *     @OA\Response(
     *         response="200",
     *         description="List of books",
     *         @OA\JsonContent(ref="#/components/schemas/Books")
     *     ),
     *     @OA\Response(
     *         response="403",
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/Unauthorized")
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthenticated",
     *         @OA\JsonContent(ref="#/components/schemas/Unauthenticated")
     *     ),
     * )
     */
    public function index()
    {
        return new BookCollectionResource(Book::paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreBookRequest $request
     * @return BookResource
     *
     * @OA\Post(
     *     path="/api/books",
     *     tags={"Books"},
     *     security={{"bearerAuth":{}}},
     *     @OA\RequestBody(
     *         description="Book data",
     *         @OA\JsonContent(
     *             type="object",
     *             ref="#/components/schemas/StoreBook"
     *         )
     *     ),
     *     @OA\Response(
     *         response="201",
     *         description="Book updated",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                 property="data",
     *                 ref="#/components/schemas/Book"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response="422",
     *         description="Validation error",
     *         @OA\JsonContent(ref="#/components/schemas/UnprocessableEntity")
     *     ),
     *     @OA\Response(
     *         response="403",
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/Unauthorized")
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthenticated",
     *         @OA\JsonContent(ref="#/components/schemas/Unauthenticated")
     *     ),
     * )
     */
    public function store(StoreBookRequest $request)
    {
        $book = $this->service->createBook($request->all());

        return new BookResource($book);
    }

    /**
     * Display the specified resource.
     *
     * @param Book $book
     * @return BookResource
     *
     * @OA\Get(
     *     path="/api/books/{book_id}",
     *     tags={"Books"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="book_id",
     *         required=true,
     *         in="path",
     *         description="ID of required book",
     *         @OA\Schema(type="integer"),
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Book",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                 property="data",
     *                 ref="#/components/schemas/Book"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Publisher not found",
     *         @OA\JsonContent(ref="#/components/schemas/NotNound")
     *     ),
     *     @OA\Response(
     *         response="403",
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/Unauthorized")
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthenticated",
     *         @OA\JsonContent(ref="#/components/schemas/Unauthenticated")
     *     ),
     * )
     */
    public function show(Book $book)
    {
        return new BookResource($book);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreBookRequest $request
     * @param Book $book
     * @return BookResource
     *
     * @OA\Put(
     *     path="/api/books/{book_id}",
     *     tags={"Books"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="book_id",
     *         required=true,
     *         in="path",
     *         description="ID of required book",
     *         @OA\Schema(type="integer"),
     *     ),
     *     @OA\RequestBody(
     *         description="Book data",
     *         @OA\JsonContent(
     *             type="object",
     *             ref="#/components/schemas/StoreBook"
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Book updated",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                 property="data",
     *                 ref="#/components/schemas/Book"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response="422",
     *         description="Validation error",
     *         @OA\JsonContent(ref="#/components/schemas/UnprocessableEntity")
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Publisher not found",
     *         @OA\JsonContent(ref="#/components/schemas/NotNound")
     *     ),
     *     @OA\Response(
     *         response="403",
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/Unauthorized")
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthenticated",
     *         @OA\JsonContent(ref="#/components/schemas/Unauthenticated")
     *     ),
     * )
     */
    public function update(StoreBookRequest $request, Book $book)
    {
        $book = $this->service->updateBook($book, $request->all());

        return new BookResource($book);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Book $book
     * @return BookResource
     * @throws Exception
     *
     * @OA\Delete(
     *     path="/api/books/{book_id}",
     *     tags={"Books"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="book_id",
     *         required=true,
     *         in="path",
     *         description="ID of required book",
     *         @OA\Schema(type="integer"),
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Book",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                 property="data",
     *                 ref="#/components/schemas/Book"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Publisher not found",
     *         @OA\JsonContent(ref="#/components/schemas/NotNound")
     *     ),
     *     @OA\Response(
     *         response="403",
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/Unauthorized")
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthenticated",
     *         @OA\JsonContent(ref="#/components/schemas/Unauthenticated")
     *     ),
     * )
     */
    public function destroy(Book $book)
    {
        $this->service->deleteBook($book);

        return new BookResource($book);
    }

    /**
     * Import books from file.
     *
     * @param ImportBooksRequest $request
     * @return BookResource
     *
     * @throws UnknownImportFileTypeException
     *
     * @OA\Post(
     *     path="/api/books/import",
     *     tags={"Books"},
     *     security={{"bearerAuth":{}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(ref="#/components/schemas/Import")
     *         ),
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Import completed",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                 property="data",
     *                 ref="#/components/schemas/ImportResponse"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response="422",
     *         description="Validation error",
     *         @OA\JsonContent(ref="#/components/schemas/UnprocessableEntity")
     *     ),
     *     @OA\Response(
     *         response="403",
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/Unauthorized")
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthenticated",
     *         @OA\JsonContent(ref="#/components/schemas/Unauthenticated")
     *     ),
     * )
     */
    public function import(ImportBooksRequest $request)
    {
        $file = $request->file('source');

        return response()->json([
            'import_count' => $this->service->import($file, $file->getClientOriginalExtension())->count(),
        ]);
    }
}
