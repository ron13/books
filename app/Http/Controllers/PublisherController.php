<?php

namespace App\Http\Controllers;

use App\Http\Resources\PublisherCollectionResource;
use App\Http\Resources\PublisherResource;
use App\Publisher;

/**
 * Class PublisherController
 * @package App\Http\Controllers
 */
class PublisherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return PublisherCollectionResource
     *
     * @OA\Get(
     *     path="/api/publishers",
     *     tags={"Publishers"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="page",
     *         in="query",
     *         description="Number of required page",
     *         @OA\Schema(type="integer"),
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="List of publishers",
     *         @OA\JsonContent(ref="#/components/schemas/Publishers")
     *     ),
     *     @OA\Response(
     *         response="403",
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/Unauthorized")
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthenticated",
     *         @OA\JsonContent(ref="#/components/schemas/Unauthenticated")
     *     ),
     * )
     */
    public function index()
    {
        return new PublisherCollectionResource(Publisher::paginate());
    }

    /**
     * Display the specified resource.
     *
     * @param Publisher $publisher
     * @return PublisherResource
     *
     * @OA\Get(
     *     path="/api/publishers/{publisher_id}",
     *     tags={"Publishers"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="publisher_id",
     *         required=true,
     *         in="path",
     *         description="ID of required publisher",
     *         @OA\Schema(type="integer"),
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Publisher",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                 property="data",
     *                 ref="#/components/schemas/Publisher"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Publisher not found",
     *         @OA\JsonContent(ref="#/components/schemas/NotNound")
     *     ),
     *     @OA\Response(
     *         response="403",
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/Unauthorized")
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthenticated",
     *         @OA\JsonContent(ref="#/components/schemas/Unauthenticated")
     *     ),
     * )
     */
    public function show(Publisher $publisher)
    {
        return new PublisherResource($publisher);
    }
}
