<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema(
 *     schema="Login",
 *     required={"email", "password"},
 *     @OA\Property(
 *          property="email",
 *          type="string",
 *          example="john.doe@mail.com"
 *     ),
 *     @OA\Property(
 *          property="password",
 *          type="string",
 *          example="password"
 *     ),
 * )
 */
class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'    => 'required|email',
            'password' => 'required|string',
        ];
    }
}
