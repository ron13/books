<?php

namespace App\Http\Requests;

use App\Services\Books\BookService;
use Illuminate\Contracts\Validation\Validator as ValidatorContract;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Validator;

/**
 * @OA\Schema(
 *     schema="StoreBook",
 *     required={"name", "isbn", "page_count", "author_id", "publisher_id"},
 *     @OA\Property(
 *          property="name",
 *          title="Book name",
 *          type="string"
 *     ),
 *     @OA\Property(
 *          property="isbn",
 *          title="Book isbn (unique worldwide book identifier)",
 *          type="string",
 *          example="661-1-71-283947-6"
 *     ),
 *     @OA\Property(
 *          property="page_count",
 *          title="Book page count",
 *          type="integer"
 *     ),
 *     @OA\Property(
 *          property="authors",
 *          title="Book author IDs",
 *          type="array",
 *          @OA\Items(type="integer")
 *     ),
 *     @OA\Property(
 *          property="publisher",
 *          title="Book publisher ID",
 *          type="integer"
 *     )
 * )
 */
class StoreBookRequest extends FormRequest
{

    /**
     * @return ValidatorContract|Validator
     */
    public function validator()
    {
        return BookService::getValidator($this->all(), strtolower($this->getMethod()) !== 'post');
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
