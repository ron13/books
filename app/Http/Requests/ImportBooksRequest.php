<?php

namespace App\Http\Requests;

use App\Rules\Extension;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ImportBooksRequest
 * @package App\Http\Requests
 */
class ImportBooksRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'source' => [
                'required',
                'file',
                new Extension(['csv', 'json']),
            ]
        ];
    }
}
