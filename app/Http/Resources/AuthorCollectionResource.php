<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * @OA\Schema(
 *     schema="Authors",
 *     type="object",
 *     @OA\Property(
 *          property="data",
 *          description="Author list",
 *          type="array",
 *          @OA\Items(ref="#/components/schemas/Author")
 *     ),
 *     @OA\Property(
 *          property="meta",
 *          ref="#/components/schemas/ListMeta"
 *     )
 * )
 */
class AuthorCollectionResource extends ResourceCollection
{
    /**
     * The resource that this resource collects.
     *
     * @var string
     */
    public $collects = AuthorResource::class;
}
