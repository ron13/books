<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property int id
 * @property string name
 *
 * @OA\Schema(
 *     schema="Author",
 *     @OA\Property(
 *          property="id",
 *          title="Author ID",
 *          type="integer"
 *     ),
 *     @OA\Property(
 *          property="name",
 *          title="Author name",
 *          type="string"
 *     ),
 * )
 */
class AuthorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'   => $this->id,
            'name' => $this->name,
        ];
    }
}
