<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * @OA\Schema(
 *     schema="Publishers",
 *     type="object",
 *     @OA\Property(
 *          property="data",
 *          description="Publisher list",
 *          type="array",
 *          @OA\Items(ref="#/components/schemas/Publisher")
 *     ),
 *     @OA\Property(
 *          property="meta",
 *          ref="#/components/schemas/ListMeta"
 *     )
 * )
 */
class PublisherCollectionResource extends ResourceCollection
{
    /**
     * The resource that this resource collects.
     *
     * @var string
     */
    public $collects = PublisherResource::class;

    /**
     * Transform the resource collection into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
