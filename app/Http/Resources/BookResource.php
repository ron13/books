<?php

namespace App\Http\Resources;

use App\Publisher;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property int id
 * @property string name
 * @property string isbn
 * @property int page_count
 * @property Collection authors
 * @property Publisher publisher
 *
 * @OA\Schema(
 *     schema="Book",
 *     @OA\Property(
 *          property="id",
 *          title="Book ID",
 *          type="integer"
 *     ),
 *     @OA\Property(
 *          property="name",
 *          title="Book name",
 *          type="string"
 *     ),
 *     @OA\Property(
 *          property="isbn",
 *          title="Book isbn (unique worldwide book identifier)",
 *          type="string",
 *          example="661-1-71-283947-6"
 *     ),
 *     @OA\Property(
 *          property="page_count",
 *          title="Book page count",
 *          type="integer"
 *     ),
 *     @OA\Property(
 *          property="authors",
 *          title="Book authors list",
 *          type="array",
 *          @OA\Items(ref="#/components/schemas/Author")
 *     ),
 *     @OA\Property(
 *          property="publisher",
 *          title="Book publisher object",
 *          ref="#/components/schemas/Publisher"
 *     )
 * )
 */
class BookResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'         => $this->id,
            'name'       => $this->name,
            'isbn'       => $this->isbn,
            'page_count' => $this->page_count,
            'authors'    => new AuthorCollectionResource($this->authors),
            'publisher'  => new PublisherResource($this->publisher),
        ];
    }
}
