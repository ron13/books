<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * @OA\Schema(
 *     schema="Books",
 *     type="object",
 *     @OA\Property(
 *          property="data",
 *          description="Book list",
 *          type="array",
 *          @OA\Items(ref="#/components/schemas/Book")
 *     ),
 *     @OA\Property(
 *          property="meta",
 *          ref="#/components/schemas/ListMeta"
 *     )
 * )
 */
class BookCollectionResource extends ResourceCollection
{
    /**
     * The resource that this resource collects.
     *
     * @var string
     */
    public $collects = BookResource::class;
}
