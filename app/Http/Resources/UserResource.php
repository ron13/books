<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property int id
 * @property string name
 * @property string email
 *
 * @OA\Schema(
 *     schema="User",
 *     @OA\Property(
 *          property="id",
 *          title="User ID",
 *          type="integer"
 *     ),
 *     @OA\Property(
 *          property="name",
 *          title="User name",
 *          type="string"
 *     ),
 *     @OA\Property(
 *          property="email",
 *          title="User email",
 *          type="string"
 *     )
 * )
 */
class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    => $this->id,
            'name'  => $this->name,
            'email' => $this->email,
        ];
    }
}
