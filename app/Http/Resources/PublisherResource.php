<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property int id
 * @property string name
 * @property string url
 *
 * @OA\Schema(
 *     schema="Publisher",
 *     @OA\Property(
 *          property="id",
 *          title="Publisher ID",
 *          type="integer"
 *     ),
 *     @OA\Property(
 *          property="name",
 *          title="Publisher name",
 *          type="string"
 *     ),
 *     @OA\Property(
 *          property="url",
 *          title="Publisher URL",
 *          type="string"
 *     )
 * )
 */
class PublisherResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'   => $this->id,
            'name' => $this->name,
            'url'  => $this->url,
        ];
    }
}
