<?php

namespace App\Events;

use Illuminate\Database\Eloquent\Model;

/**
 * Interface ModelEventInterface
 * @package App\Events
 */
interface ModelEventInterface
{
    /**
     * @return string
     */
    public function getType(): string;

    /**
     * @return Model|null
     */
    public function getModel();
}
