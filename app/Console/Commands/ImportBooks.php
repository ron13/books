<?php

namespace App\Console\Commands;

use App\Exceptions\UnknownImportFileTypeException;
use App\Services\Books\BookService;
use Illuminate\Console\Command;
use Illuminate\Http\File;
use Symfony\Component\Console\Output\ConsoleOutput;

/**
 * Class ImportBooks
 * @package App\Console\Commands
 */
class ImportBooks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import books from file';
    /**
     * @var BookService
     */
    private $service;

    /**
     * Create a new command instance.
     *
     * @param BookService $service
     */
    public function __construct(BookService $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    /**
     * Execute the console command.
     *
     * @throws UnknownImportFileTypeException
     */
    public function handle()
    {
        $filePath = $this->argument('file');
        $file = new File($filePath);
        $count = $this->service->import($file, $file->getExtension())->count();

        $console = new ConsoleOutput();
        $console->writeln(__('Import :file file completed.', [
            'file'  => $filePath,
        ]));
        $console->writeln(__(':count Books added to the database.', [
            'count' => $count,
        ]));
    }
}
