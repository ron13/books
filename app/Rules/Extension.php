<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\UploadedFile;

/**
 * Class Extension
 * @package App\Rules
 */
class Extension implements Rule
{
    /**
     * @var array
     */
    private $extensions;

    /**
     * Create a new rule instance.
     *
     * @param array $extensions
     */
    public function __construct(array $extensions)
    {
        $this->extensions = $extensions;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!($value instanceof UploadedFile)) {
            return false;
        }
        /** @var UploadedFile $value */
        return in_array($value->getClientOriginalExtension(), $this->extensions);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('The :attribute must be a file with extension: :extensions.', [
            'extensions' => implode(', ', $this->extensions),
        ]);
    }
}
