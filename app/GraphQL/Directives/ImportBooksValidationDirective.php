<?php

namespace App\GraphQL\Directives;

use App\Rules\Extension;
use App\Services\Books\BookService;
use Nuwave\Lighthouse\Schema\Directives\ValidationDirective;

/**
 * Class StoreBookValidationDirective
 * @package App\GraphQL\Directives
 */
class ImportBooksValidationDirective extends ValidationDirective
{

    /**
     * Name of the directive as used in the schema.
     *
     * @return string
     */
    public function name()
    {
        return 'storeBookValidation';
    }

    /**
     * Return validation rules for the arguments.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'source' => [
                'required',
                'file',
                new Extension(['csv', 'json']),
            ]
        ];
    }

}
