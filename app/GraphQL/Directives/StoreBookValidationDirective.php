<?php

namespace App\GraphQL\Directives;

use App\Services\Books\BookService;
use Nuwave\Lighthouse\Schema\Directives\ValidationDirective;

/**
 * Class StoreBookValidationDirective
 * @package App\GraphQL\Directives
 */
class StoreBookValidationDirective extends ValidationDirective
{

    /**
     * Name of the directive as used in the schema.
     *
     * @return string
     */
    public function name()
    {
        return 'storeBookValidation';
    }

    /**
     * Return validation rules for the arguments.
     *
     * @return array
     */
    public function rules(): array
    {
        $validator = BookService::getValidator($this->args, $this->name() == 'updateBook');
        return $validator->getRules();
    }

}
