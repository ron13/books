<?php

namespace App\GraphQL\Mutations;

use App\Book;
use App\Exceptions\UnknownImportFileTypeException;
use App\Services\Books\BookService;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Http\UploadedFile;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

/**
 * Class UpdateBook
 * @package App\GraphQL\Mutations
 */
class ImportBooks
{

    /**
     * @var BookService
     */
    private $service;

    /**
     * CreateBook constructor.
     * @param BookService $service
     */
    public function __construct(BookService $service)
    {
        $this->service = $service;
    }

    /**
     * Return a value for the field.
     *
     * @param null $rootValue Usually contains the result returned from the parent field. In this case,
     * it is always `null`.
     * @param mixed[] $args The arguments that were passed into the field.
     * @param GraphQLContext $context Arbitrary data that is shared between all fields of a single query.
     * @param ResolveInfo $resolveInfo Information about the query itself,
     * such as the execution state, the field name, path to the field from the root, and more.
     * @return mixed
     * @throws UnknownImportFileTypeException
     */
    public function __invoke($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        /** @var UploadedFile $file */
        $file = $args['source'];

        return $this->service->import($file, $file->getClientOriginalExtension())->count();
    }
}
