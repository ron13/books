<?php

/** @var Factory $factory */

use App\Book;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Book::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence(4),
        'isbn' => $faker->unique()->numerify('111-1-11-######-1'),
        'page_count' => $faker->numberBetween(50, 1500),
    ];
});
