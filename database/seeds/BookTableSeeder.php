<?php

use App\Publisher;
use Illuminate\Database\Seeder;

class BookTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Book::class, 20)->make()->each(function (\App\Book $book) {
            /** @var \App\Author $author */
            $authors   = \App\Author::query()->inRandomOrder()->limit(rand(1,3))->get();
            /** @var Publisher $publisher */
            $publisher = Publisher::query()->inRandomOrder()->first();

            $book->publisher()->associate($publisher);

            $book->save();

            $book->authors()->sync($authors);
        });
    }
}
