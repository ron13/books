<?php

use Illuminate\Database\Seeder;

class AuthorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Author::class, 5)->make()->each(function (\App\Author $author, $key) {
            $author->name = "Author {$key}";
            $author->save();
        });
    }
}
