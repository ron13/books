<?php

use App\Publisher;
use Illuminate\Database\Seeder;

class PublisherTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Publisher::class, 3)->make()->each(function (Publisher $publisher, $key) {
            $publisher->name = "Publisher {$key}";
            $publisher->save();
        });
    }
}
