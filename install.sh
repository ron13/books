#!/bin/bash

ENV=.env
if [ ! -f "$FILE" ]; then
  cp .env.example .env
  php artisan key:generate --ansi
  php artisan jwt:secret
  php artisan migrate:fresh --seed
fi
